---
title: Scribe.title

language_tabs:
- bash
- javascript

includes:
- "./prepend.md"
- "./authentication.md"
- "./groups/*"
- "./errors.md"
- "./append.md"

logo: 

toc_footers:
- <a href="./collection.json">View Postman collection</a>
- <a href="./openapi.yaml">View OpenAPI (Swagger) spec</a>
- <a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a>

---

# Introduction

Scribe.description

Scribe.intro_text

<aside>Scribe.intro_text.aside </aside>

<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "http://scribe.laraveldev.com";
</script>
<script src="js/tryitout-2.4.2.js"></script>

> Base URL

```yaml
http://scribe.laraveldev.com
```
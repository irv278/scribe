<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


/**
 * @group Scribe
 *
 * Documenting your API
 *
 */


class ScribeController extends Controller
{
    /**
     * getApiSample discription
     *
     *
     * @authenticated
     *
     * @urlParam id integer required The ID of the post.
     * @urlParam mobile number required The mobile of the author.
     * @urlParam name string required The name of the author.
     *
     * @queryParam product_id int required 商品 id 值
     * @queryParam product_name string required 商品名稱
     * @queryParam product_barcode number required 商品名稱
     * @queryParam is_useful boolean required 商品名稱
     * @queryParam cars integer[] integer[]測試
     * @bodyParam cars object 測試
     * @queryParam cars integer[] integer[]測試
     * @queryParam sort string Field to sort by. Defaults to 'id'.
     * @queryParam fields required Comma-separated list of fields to include in the response. Example: title,published_at,is_public
     * @queryParam filters[published_at] Filter by date published.
     * @queryParam filters[is_public] integer Filter by whether a post is public or not. Example: 1
     *
     * 測試文字
     *
     * @response {
     *  "id": 4,
     *  "name": "Jessica Jones",
     *  "roles": ["admin"]
     * }
     */
    public function getApiSample($id)
    {
        return "id -> " . $id;
    }

    /**
     * postApiSample discription
     */
    public function postApiSample()
    {
        return  __METHOD__;
    }

    public function putApiSample()
    {
        return  __METHOD__;
    }

    public function patchApiSample()
    {
        return  __METHOD__;
    }

    public function deleteApiSample()
    {
        return  __METHOD__;
    }

    public function optionsApiSample()
    {
        return  __METHOD__;
    }
}

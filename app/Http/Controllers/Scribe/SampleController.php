<?php

namespace App\Http\Controllers\Scribe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


/**
 * @group Scribe
 *
 * Documenting your API
 */

class SampleController extends Controller
{
    public function getScribeSample()
    {
        return  __METHOD__;
    }
}

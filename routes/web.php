<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('/api')->namespace('App\Http\Controllers\Api')->group(function () {

    Route::get('/get-sample/{id}', 'ScribeController@getApiSample');

    Route::post('/post-sample', 'ScribeController@postApiSample');

    Route::put('/put-sample', 'ScribeController@putApiSample');

    Route::patch('/patch-sample', 'ScribeController@patchApiSample');

    Route::delete('/delete-sample', 'ScribeController@deleteApiSample');

    Route::options('/options-sample', 'ScribeController@optionsApiSample');
});

Route::prefix('/scribe')->namespace('App\Http\Controllers\Scribe')->group(function () {

    Route::get('/get-sample', 'SampleController@getScribeSample');
});


